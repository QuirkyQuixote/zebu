
%require "3.2"
%debug
%language "c++"
%define api.token.constructor
%define api.value.type variant
%define api.location.file none
%define parse.assert
%param { zz::ast32& ast }
%locations

// *.h
%code requires {
#include "zebu4/zebu_cpp.h"

enum Token { null, True, False, array, object, error };

}

// *.cc
%code {
#include <climits>  // INT_MIN, INT_MAX
#include <iostream>
#include <sstream>

namespace yy {
        // Prototype of the yylex function providing subsequent tokens.
        static parser::symbol_type yylex(zz::ast32& ast);
}; // namespace yy

}

%token<zz::ref32> VALUE
%token<zz::ref32> STRING
%token<zz::ref32> NUMBER
%token<zz::ref32> COMMENT
%token COLON ':';
%token COMMA ',';
%token LBRACKET '[';
%token RBRACKET ']';
%token LCURLY '{';
%token RCURLY '}';
%token END_OF_FILE 0;
%token ERROR

%type<zz::ref32> expr
%type<std::vector<zz::ref32>> expr_list
%type<std::vector<zz::ref32>> pair_list
%type<zz::ref32> pair

%%

input
        : expr { std::cout << zz::emit(ast, $1) << '\n'; }
        ;

expr
        : VALUE                         { $$ = $1; }
        | STRING                        { $$ = $1; }
        | NUMBER                        { $$ = $1; }
        | LBRACKET expr_list RBRACKET   { $$ = ast.push($2.begin(), $2.end()); }
        | LBRACKET RBRACKET             { $$ = ast.push({zz::ref32{Token::array}}); }
        | LCURLY pair_list RCURLY       { $$ = ast.push($2.begin(), $2.end()); }
        | LCURLY RCURLY                 { $$ = ast.push({zz::ref32{Token::object}}); }
        ;

expr_list
        : expr_list COMMA expr { $$ = std::move($1); $$.push_back($3); }
        | expr { $$ = {zz::ref32{Token::array}, $1}; }
        ;

pair_list
        : pair_list COMMA pair { $$ = std::move($1); $$.push_back($3); }
        | pair { $$ = {zz::ref32{Token::object}, $1}; }
        ;

pair
        : STRING COLON expr { $$ = ast.push({$1, $3}); }

%%

#include <cstring>

#include "json.h"

namespace yy {

static parser::symbol_type yylex(zz::ast32& ast)
{
        static unsigned line_num = 0;
        static std::string line;
        static char *cur = &line[0];
        for (;;) {
                if (*cur == 0) {
                        if (!std::getline(std::cin, line)) {
                                parser::location_type loc{nullptr, line_num, 0};
                                return parser::make_END_OF_FILE(std::move(loc));
                        }
                        cur = &line[0];
                        ++line_num;
                } else if (isspace(*cur)) {
                        ++cur;
                } else {
                        break;
                }
        }
        parser::location_type loc{nullptr, line_num, (unsigned)(cur - &line[0] + 1)};
        char *next;
        if (*cur == ':') {
                ++cur;
                return parser::make_COLON(std::move(loc));
        } else if (*cur == ',') {
                ++cur;
                return parser::make_COMMA(std::move(loc));
        } else if (*cur == '[') {
                ++cur;
                return parser::make_LBRACKET(std::move(loc));
        } else if (*cur == ']') {
                ++cur;
                return parser::make_RBRACKET(std::move(loc));
        } else if (*cur == '{') {
                ++cur;
                return parser::make_LCURLY(std::move(loc));
        } else if (*cur == '}') {
                ++cur;
                return parser::make_RCURLY(std::move(loc));
        } else if (memcmp(cur, "null", 4) == 0) {
                cur += 4;
                return parser::make_VALUE(zz::ref32{Token::null}, std::move(loc));
        } else if (memcmp(cur, "true", 4) == 0) {
                cur += 4;
                return parser::make_VALUE(zz::ref32{Token::True}, std::move(loc));
        } else if (memcmp(cur, "false", 5) == 0) {
                cur += 5;
                return parser::make_VALUE(zz::ref32{Token::False}, std::move(loc));
        } else if ((next = parse_number(cur))) {
                double n = strtod(cur, NULL);
                cur = next;
                return parser::make_NUMBER(ast.push(n), std::move(loc));
        } else if ((next = parse_string(cur))) {
                ++cur;
                std::string_view v(cur, next - cur - 1);
                cur = next;
                return parser::make_STRING(ast.push(v), std::move(loc));
        }
        return parser::make_ERROR(std::move(loc));
}

void parser::error (const parser::location_type& loc, const std::string& msg)
{ std::cerr << loc << ": " << msg << '\n'; }

}; // namespace yy

int main(int argc, const char *argv[])
{
        zz::ast32 ast;
        auto&& p = yy::parser{ast};
        p.set_debug_level (!!getenv ("YYDEBUG"));
        return p.parse ();
}

