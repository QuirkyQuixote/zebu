%code requires{
#include "zebu4/zebu.h"
enum Token { TOK_NULL, TOK_TRUE, TOK_FALSE, TOK_ARRAY, TOK_OBJECT };
}

%code provides{
int yylex(YYSTYPE* lvalp, YYLTYPE* llocp, struct zz_ast* ast);
void yyerror(YYLTYPE* llocp, struct zz_ast* ast, const char *msg);
}


%define api.pure full
%locations
%param {struct zz_ast* ast}
%union {
        size_t pos;
        struct zz_buf *buf;
}        
%define parse.error verbose

%token ERROR
%token<pos> VALUE
%token<pos> STRING
%token<pos> NUMBER
%token<pos> COMMENT

%type<pos> expr
%type<buf> expr_list
%type<buf> pair_list
%type<pos> pair

%%

input
        : expr { zz_println(ast, $1, stdout); }
        ;

expr
        : VALUE                 { $$ = $1; }
        | STRING                { $$ = $1; }
        | NUMBER                { $$ = $1; }
        | '[' expr_list ']'     { $$ = zz_push_buf(ast, $2); }
        | '[' ']'               { $$ = zz_push_list(ast, TOK_ARRAY); }
        | '{' pair_list '}'     { $$ = zz_push_buf(ast, $2); }
        | '{' '}'               { $$ = zz_push_list(ast, TOK_OBJECT); }
        ;

expr_list
        : expr_list ',' expr { $$ = zz_append($1, $3); }
        | expr { $$ = zz_append(zz_append(NULL, TOK_ARRAY), $1); }
        ;

pair_list
        : pair_list ',' pair { $$ = zz_append($1, $3); }
        | pair { $$ = zz_append(zz_append(NULL, TOK_OBJECT), $1); }
        ;

pair
        : STRING ':' expr { $$ = zz_push_list(ast, $1, $3); }

%%

#include <ctype.h>

#include "json.h"

int yylex(YYSTYPE* lvalp, YYLTYPE* llocp, struct zz_ast* ast)
{
        static long line_num = 0;
        static char buf[1024] = "";
        static char *cur = buf;
        for (;;) {
                if (*cur == 0 || *cur == '\n') {
                        if (fgets(buf, sizeof(buf), stdin) == NULL) {
                                llocp->first_line = line_num;
                                llocp->first_column = 1;
                                return EOF;
                        }
                        cur = buf;
                        ++line_num;
                } else if (isspace(*cur)) {
                        ++cur;
                } else {
                        break;
                }
        }
        llocp->first_line = line_num;
        llocp->first_column = cur - buf + 1;
        char *next;
        if (memcmp(cur, "null", 4) == 0) {
                cur += 4;
                lvalp->pos = TOK_NULL;
                return VALUE;
        } else if (memcmp(cur, "true", 4) == 0) {
                cur += 4;
                lvalp->pos = TOK_TRUE;
                return VALUE;
        } else if (memcmp(cur, "false", 5) == 0) {
                cur += 5;
                lvalp->pos = TOK_FALSE;
                return VALUE;
        } else if ((next = parse_number(cur))) {
                lvalp->pos = zz_push_real(ast, strtod(cur, NULL));
                cur = next;
                return NUMBER;
        } else if ((next = parse_string(cur))) {
                ++cur;
                lvalp->pos = zz_push_string(ast, cur, next - cur - 1);
                cur = next;
                return STRING;
        } else {
                return *cur++;
        }
}

void yyerror(YYLTYPE* llocp, struct zz_ast* ast, const char *msg)
{
        fprintf(stderr, "%d,%d:%s\n", llocp->first_line, llocp->first_column, msg);
}

int main(int argc, const char *argv[])
{
        struct zz_ast ast = zz_ast();
        return yyparse(&ast);
}

