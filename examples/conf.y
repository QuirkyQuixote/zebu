%code requires{
#include "zebu4/zebu.h"
enum Token { TOK_LINE, TOK_COMMENT, TOK_ERROR };
}

%code provides{
int yylex(YYSTYPE* lvalp, YYLTYPE* llocp, struct zz_ast* ast);
void yyerror(YYLTYPE* llocp, struct zz_ast* ast, const char *msg);
}

%define api.pure full
%locations
%param {struct zz_ast* ast}
%union {
        size_t pos;
        struct zz_buf *buf;
}
%define parse.error verbose

%token '='
%token '['
%token ']'
%token '\n'
%token<pos> BLOCK
%token<pos> KEY
%token<pos> VALUE
%token<pos> COMMENT
%token ERROR

%type<pos> input
%type<buf> blocks
%type<pos> block
%type<buf> lines
%type<pos> line

%%

input
        : lines blocks {
                zz_println(ast, zz_push_buf(ast, zz_prepend($2, zz_push_buf(ast, $1))), stdout); }
        ;

blocks
        : blocks block { $$ = zz_append($1, $2); }
        | { $$ = NULL; }
        ;

block
        : BLOCK lines {
                $$ = zz_push_list(ast, $1, zz_push_buf(ast, $2)); }
        | error {
                $$ = TOK_ERROR; }
        ;

lines
        : lines line { $$ = zz_append($1, $2); }
        | { $$ = NULL; }
        ;

line
        : KEY VALUE {
                $$ = zz_push_list(ast, TOK_LINE, $1, $2); }
        | COMMENT {
                $$ = zz_push_list(ast, TOK_COMMENT, $1); }
        | error {
                $$ = TOK_ERROR; }
        ;

%%

#include <ctype.h>

int yylex(YYSTYPE* lvalp, YYLTYPE* llocp, struct zz_ast* ast)
{
        static long line_num = 0;
        static char buf[1024] = "";
        static char *cur = buf;
        for (;;) {
                if (*cur == 0 || *cur == '\n') {
                        if (fgets(buf, sizeof(buf), stdin) == NULL) {
                                llocp->first_line = line_num;
                                llocp->first_column = 1;
                                return EOF;
                        }
                        cur = buf;
                        ++line_num;
                } else if (isspace(*cur)) {
                        ++cur;
                } else {
                        break;
                }
        }

        llocp->first_line = line_num;
        llocp->first_column = cur - buf + 1;

        if (*cur == '[') {
                ++cur;
                char *end = strchr(cur, ']');
                if (end == NULL) return ERROR;
                lvalp->pos = zz_push_string(ast, cur, end - cur);
                cur = end + 1;
                return BLOCK;
        } else if (*cur == '=') {
                do ++cur; while (isspace(*cur));
                char *end = cur + strlen(cur);
                while (cur != end && isspace(*(end - 1))) --end;
                lvalp->pos = zz_push_string(ast, cur, end - cur);
                cur = end;
                return VALUE;
        } else if (*cur == '#') {
                ++cur;
                size_t len = strlen(cur);
                lvalp->pos = zz_push_string(ast, cur, len);
                cur += len;
                return COMMENT;
        } else {
                char* end = strpbrk(cur, " \t\r\n=");
                if (end == NULL) end = cur + strlen(cur);
                lvalp->pos = zz_push_string(ast, cur, end - cur);
                cur = end;
                return KEY;
        }
        return ERROR;
}

void yyerror(YYLTYPE* llocp, struct zz_ast* ast, const char *msg)
{
        fprintf(stderr, "%d,%d:%s\n", llocp->first_line, llocp->first_column, msg);
}

int main(int argc, const char *argv[])
{
        struct zz_ast ast = zz_ast();
        return yyparse(&ast);
}

