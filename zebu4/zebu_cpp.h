#ifndef ZZ_CPP_H_
#define ZZ_CPP_H_

#include <algorithm>
#include <stdexcept>
#include <vector>
#include <type_traits>
#include <ostream>

#define ZZ_CPP_VERSION "4.1.0"
#define ZZ_CPP_VERSION_MAJOR 4
#define ZZ_CPP_VERSION_MINOR 1
#define ZZ_CPP_VERSION_PATCH 0

namespace zz {

template<class Base> struct Ref {
 public:
        enum Type : Base {
                tinyint     = static_cast<Base>(0) << (sizeof(Base) * 8 - 4),
                integer     = static_cast<Base>(1) << (sizeof(Base) * 8 - 4),
                real        = static_cast<Base>(2) << (sizeof(Base) * 8 - 4),
                string      = static_cast<Base>(3) << (sizeof(Base) * 8 - 4),
                list        = static_cast<Base>(4) << (sizeof(Base) * 8 - 4),
                mask        = tinyint | integer | real | string | list,
        };

        enum Npos : Base {
                npos        = static_cast<Base>(-1),
        };

 private:
        Base _{npos};

 public:
        Ref(Base pos) : _{pos} {}
        Ref() = default;
        Ref(const Ref&) = default;
        Ref(Ref&&) = default;

        ~Ref() = default;

        Ref& operator=(const Ref&) = default;
        Ref& operator=(Ref&&) = default;

        operator bool() const { return _ != npos; }
        Base get() const { return _; }
        Base index() const { return _ & ~mask; }
        Base type() const { return _ & mask; }
        Ref operator|(Base type) const { return Ref{_ | type}; }
};

template<class Base> class Iterator {
 public:
        using value_type = Ref<Base>;
        using reference = value_type;
        using pointer = value_type*;
        using difference_type = ptrdiff_t;
        using iterator_category = std::random_access_iterator_tag;

 private:
        const value_type* pos_{nullptr};

 public:
        Iterator(const value_type* pos) : pos_{pos} {}
        Iterator() = default;
        Iterator(const Iterator&) = default;
        Iterator(Iterator&&) = default;

        ~Iterator() = default;

        Iterator& operator=(const Iterator&) = default;
        Iterator& operator=(Iterator&&) = default;

        value_type operator*() { return *pos_; }
        value_type operator[](difference_type n) { return *(pos_ + n); }

        Iterator& operator++() { ++pos_; return *this; }
        Iterator& operator--() { --pos_; return *this; }
        Iterator operator++(int) { auto r = *this; ++pos_; return r; }
        Iterator operator--(int) { auto r = *this; --pos_; return r; }
        Iterator& operator+=(difference_type n) { pos_ += n; return *this; }
        Iterator& operator-=(difference_type n) { pos_ -= n; return *this; }
        Iterator operator+(difference_type n) const { return Iterator{pos_ + n}; }
        Iterator operator-(difference_type n) const { return Iterator{pos_ - n}; }
        difference_type operator-(const Iterator& r) { return pos_ - r.pos_; }

        bool operator==(const Iterator& r) const { return pos_ == r.pos_; }
        bool operator!=(const Iterator& r) const { return pos_ != r.pos_; }
        bool operator<(const Iterator& r) const { return pos_ < r.pos_; }
        bool operator<=(const Iterator& r) const { return pos_ <= r.pos_; }
        bool operator>(const Iterator& r) const { return pos_ > r.pos_; }
        bool operator>=(const Iterator& r) const { return pos_ >= r.pos_; }
};

template<class Base>
Iterator<Base> operator+(typename Iterator<Base>::difference_type n, const Iterator<Base>& it)
{ return it + n; }

template<class Base> class List {
 public:
        using value_type = Ref<Base>;
        using reference = value_type;
        using pointer = value_type*;
        using size_type = size_t;
        using difference_type = ptrdiff_t;
        using iterator = Iterator<Base>;

 private:
        const value_type* data_;

 public:
        List(const value_type* data) : data_{data} {}

        value_type operator[](size_type n) const { return data_[n + 1]; }
        size_type size() const { return data_[0].get(); }
        bool empty() const { return size() == 0; }

        iterator begin() const { return iterator{data_ + 1}; }
        iterator end() const { return iterator{data_ + 1 + size()}; }
};

template<size_t NBytes> struct Integer {};
template<> struct Integer<2> { using type = int16_t; };
template<> struct Integer<4> { using type = int32_t; };
template<> struct Integer<8> { using type = int64_t; };

template<size_t NBytes> struct Real {};
template<> struct Real<4> { using type = float; };
template<> struct Real<8> { using type = double; };

template<class Base, class T, class Enable = void> struct Traits {};

template<class Base, class T>
struct Traits<Base, T, std::enable_if_t<std::is_integral_v<T>>> {
        using ref = Ref<Base>;
        using integer = typename Integer<sizeof(Base)>::type;

        static ref push(std::vector<ref>& data, integer value)
        {
                if constexpr (sizeof(T) < sizeof(Base)) return ref{value};
                Base pos = data.size();
                data.push_back(value);
                return ref{pos | ref::integer};
        }

        static integer get(const std::vector<ref>& data, ref pos)
        {
                if (pos.type() == ref::tinyint) return pos.index();
                if (pos.type() == ref::integer)
                        return static_cast<integer>(data[pos.index()]);
                throw std::runtime_error{"expected integer"};
        }
};

template<class Base, class T>
struct Traits<Base, T, std::enable_if_t<std::is_enum_v<T>>> {
        using ref = Ref<Base>;
        using integer = typename Integer<sizeof(Base)>::type;

        static ref push(std::vector<ref>& data, integer value)
        {
                if constexpr (sizeof(T) < sizeof(Base)) return ref{value};
                Base pos = data.size();
                data.push_back(value);
                return ref{pos | ref::integer};
        }

        static integer get(const std::vector<ref>& data, ref pos)
        {
                if (pos.type() == ref::tinyint) return pos.index();
                if (pos.type() == ref::integer)
                        return static_cast<integer>(data[pos.index()]);
                throw std::runtime_error{"expected integer"};
        }
};

template<class Base, class T>
struct Traits<Base, T, std::enable_if_t<std::is_floating_point_v<T>>> {
        using ref = Ref<Base>;
        using real = typename Real<sizeof(Base)>::type;

        static ref push(std::vector<ref>& data, real value)
        {
                Base pos = data.size();
                data.push_back(*reinterpret_cast<Base*>(&value));
                return pos | ref::real;
        }

        static real get(const std::vector<ref>& data, ref pos)
        {
                if (pos.type() == ref::real)
                        return *reinterpret_cast<const real*>(&data[pos.index()]);
                throw std::runtime_error{"expected real"};
        }
};

template<class Base> struct Traits<Base, std::string_view> {
        using ref = Ref<Base>;

        static ref push(std::vector<ref>& data, const std::string_view& str)
        {
                Base pos = data.size();
                size_t len = str.size() / sizeof(Base) + 1;
                auto buf = reinterpret_cast<const Base*>(str.data());
                data.insert(data.end(), buf, buf + len);
                reinterpret_cast<char *>(&data[pos])[str.size()] = 0;
                return pos | ref::string;
        }

        static std::string_view get(const std::vector<ref>& data, ref pos)
        {
                if (pos.type() == ref::string)
                        return reinterpret_cast<const char*>(&data[pos.index()]);
                throw std::runtime_error{"expected string"};
        }
};

template<class Base> struct Traits<Base, std::string> {
        using ref = Ref<Base>;

        static ref push(std::vector<ref>& data, const std::string& value)
        { return Traits<Base, std::string_view>::push(data, std::string_view(value.data())); }

        static std::string get(const std::vector<ref>& data, ref pos)
        { return Traits<Base, std::string_view>::get(data, pos); }
};

template<class Base> struct Traits<Base, char*> {
        using ref = Ref<Base>;

        static ref push(std::vector<ref>& data, const char* value)
        { return Traits<Base, std::string_view>::push(data, std::string_view(value)); }

        static const char* get(const std::vector<ref>& data, ref pos)
        { return Traits<Base, std::string_view>::get(data, pos).data(); }
};

template<class Base> struct Traits<Base, const char*> {
        using ref = Ref<Base>;

        static ref push(std::vector<ref>& data, const char* value)
        { return Traits<Base, std::string_view>::push(data, std::string_view(value)); }

        static const char* get(const std::vector<ref>& data, ref pos)
        { return Traits<Base, std::string_view>::get(data, pos).data(); }
};

template<class Base> struct Traits<Base, List<Base>> {
        using ref = Ref<Base>;

        static ref push(std::vector<ref>& data, const List<Base>& list)
        {
                Base pos = data.size();
                data.push_back(Base{list.size()});
                data.insert(data.end(), list.begin(), list.end());
                return pos | ref::list;
        }

        static List<Base> get(const std::vector<ref>& data, ref pos)
        {
                if (pos.type() == ref::list) return List{&data[pos.index()]};
                throw std::runtime_error{"expected list"};
        }
};

template<class Base = uint32_t> class Ast {
 public:
        using value_type = Ref<Base>;
        using reference = value_type;
        using pointer = value_type*;
        using size_type = size_t;
        using difference_type = ptrdiff_t;

 private:
        std::vector<value_type> data_;

 public:
        template<class T> value_type push(const T& value)
        { return Traits<Base, std::decay_t<T>>::push(data_, value); }

        template<class Iter> value_type push(Iter first, Iter last)
        {
                Base pos = data_.size();
                data_.push_back(last - first);
                data_.insert(data_.end(), first, last);
                return value_type{pos | value_type::list};
        }

        value_type push(std::initializer_list<value_type> list)
        {
                Base pos = data_.size();
                data_.push_back(list.size());
                data_.insert(data_.end(), list.begin(), list.end());
                return value_type{pos | value_type::list};
        }

        template<class T, class Base2> friend T get(const Ast<Base2>& ast, Ref<Base2> pos);
};

template<class T, class Base> T get(const Ast<Base>& ast, Ref<Base> pos)
{ return Traits<Base, std::decay_t<T>>::get(ast.data_, pos); }

template<class Base> struct Emit {
        const Ast<Base>& ast;
        Ref<Base> ref;
};

template<class Base> Emit<Base> emit(const Ast<Base>& ast, Ref<Base> ref)
{ return Emit<Base>{ast, ref}; }

template<class Base> std::ostream& operator<<(std::ostream& out, const Emit<Base>& e)
{
        if (!e.ref) {
                out << "()";
        } else if (e.ref.type() == Ref<Base>::integer || e.ref.type() == Ref<Base>::tinyint) {
                out << get<int>(e.ast, e.ref);
        } else if (e.ref.type() == Ref<Base>::real) {
                out << get<double>(e.ast, e.ref);
        } else if (e.ref.type() == Ref<Base>::string) {
                out << '"' << get<const char*>(e.ast, e.ref) << '"';
        } else if (e.ref.type() == Ref<Base>::list) {
                List<Base> list = get<List<Base>>(e.ast, e.ref);
                out << '(';
                if (!list.empty()) {
                        out << emit(e.ast, list[0]);
                        for (size_t i = 1; i != list.size(); ++i)
                                out << ' ' << emit(e.ast, list[i]);
                }
                out << ')';
        }
        return out;
}

using ast16 = Ast<uint16_t>;
using ast32 = Ast<uint32_t>;
using ast64 = Ast<uint64_t>;

using ref16 = Ref<uint16_t>;
using ref32 = Ref<uint32_t>;
using ref64 = Ref<uint64_t>;

using list16 = List<uint16_t>;
using list32 = List<uint32_t>;
using list64 = List<uint64_t>;

}; // namespace zz

#endif // ZZ_CPP_H_


