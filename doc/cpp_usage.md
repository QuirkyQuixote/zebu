# C++ Usage

## Requirements

To use the C++ implementation you must include `zebu4/zebu_cpp.h`.

```cpp
#include "zebu4/zebu_cpp.h"
```

The `zz::Ast<>` template class manages a whole tree; aliases are provided for
16, 32, and 64-bit implementations.

```cpp
zz::ast32 ast;
// do things with ast
// it'll be automatically destroyed at end of scope
```

## Constructing a Tree

Once the tree is created, you can push nodes into it and obtain handles to
them with the `push()` functions.

```cpp
zz::ref32 integer = ast.push(42);
zz::ref32 real = ast.push(3.141592);
zz::ref32 string = ast.push("Hello, world!");
zz::ref32 list = ast.push({integer, real, string});
```

In addition to the list-building `push()` that takes
`std::initializer_list<zz::Ref>`, there is another that takes a pair of
iterators defining a range.

```cpp
std::vector<zz::ref32> tmp{integer, real, string};
zz::ref32 list = ast.push(tmp.begin(), tmp.end());
```

You can also use non-negative integers or enum values mapped to non-negative
integers in place of `zz::Ref` instances, and they'll become integer values in
the tree.

```cpp
zz::ref32 list = ast::push({zz::ref32{Token::add}, zz::ref32{1}, zz::ref32{2}});
```

## Navigating a Tree

To retrieve the values stored in the tree, use the `zz::Ast::get()` functions.

```cpp
std::cout << ast.get<int>(integer) << '\n';
std::cout << ast.get<double>(real) << '\n';
std::cout << ast.get<const char*>(string) << '\n';
```

Specifically, retrieving a list will return a `zz::List` object that can be
used to iterate over the list values.

```cpp
zz::list32 l{ast.get<zz::list32>(list)};
std::cout << l.size << " elements returned\n";
for (auto x : l) {
        /* do things with the values of list */
}
```

