# C++ API

## Base type

The base type determines the size of the nodes in the tree; it gives an upper
size to the actual tree, but also to the numeric values stored on it.

By default, the base is set to `uint32_t`, and that gives a tree with up to
`2^32` nodes that can store integers and reals with up to 32 bits.

All the following templates have Base as their first argument.

## zz::Ref

Handle for a node in an Ast

```cpp
template<class Base> class Ref;
```

### Member Types

```cpp
enum Type : Base {
        tinyint = static_cast<Base>(0x0) << (sizeof(Base) * 8 - 4),
        integer = static_cast<Base>(0x1) << (sizeof(Base) * 8 - 4),
        real    = static_cast<Base>(0x2) << (sizeof(Base) * 8 - 4),
        string  = static_cast<Base>(0x3) << (sizeof(Base) * 8 - 4),
        list    = static_cast<Base>(0x4) << (sizeof(Base) * 8 - 4),
        mask    = static_cast<Base>(0xF) << (sizeof(Base) * 8 - 4),
};
```

### Related Functions

```cpp
Ref::Ref(Base base);
Ref::Ref();
Ref::Ref(const Ref&);
Ref::Ref(Ref&&);
```

### Assignment

```cpp
Ref::Ref(const Ref&);
Ref::Ref(Ref&&);
```

### Decomposition

```cpp
Ref::operator bool() const { return _ != npos; }
Base Ref::get() const { return _; }
Base Ref::index() const { return _ & ~mask; }
Base Ref::type() const { return _ & mask; }
Ref Ref::operator|(Base type) const { return Ref{_ | type}; }
```

1. Casting to `bool` returns `true` if the node is not empty.
2. Returns the internal representation.
3. The `index()` function returns the index of the element referenced by `ref`.
4. The `type()` function returns a the type of the data referenced by `ref`.
5. A `Ref` object is usually built by bitmasking together the index and type.

## zz::List

Non-owning reference to a fixed-size list of nodes in an Ast.

```cpp
template<class Base> class List;
```

### Member Types

```cpp
using value_type = Ref<Base>;
using reference = Ref<Base>;
using pointer = Ref<Base>*;
using size_type = size_t;
using difference_type = ptrdiff_t;
using iterator = Iterator;
```

### Member Functions

```cpp
List::List()
List::List(const List& other)
List::List(List&& other)
```

Construct a new List

### Assignment

```cpp
List& List::operator=(const List& other)
List& List::operator=(List&& other)
```

Assigns the referenced list of another

### Iterators

```cpp
iterator List::begin() const
iterator List::end() const
```

### Element Access

```cpp
Ref List::operator[](size_type n) const
```

### Size

```cpp
size_type List::size() const
bool List::empty() const
```

## zz::Ast

Actual tree.

```cpp
template<class Base> class Ast;
```

### Member Types

```cpp
using value_type = Ref<Base>;
using reference = Ref<Base>;
using pointer = Ref<Base>*;
using size_type = size_t;
using difference_type = ptrdiff_t;
```

### Member Functions

```cpp
Ast::Ast()
Ast::Ast(const Ast& other)
Ast::Ast(Ast&& other)
```

### Assignment

```cpp
Ast& Ast::operator=(const Ast& other)
Ast& Ast::operator=(Ast&& other)
```

### Insertion

```cpp
template<class T> Ref<Base> Ast::push(const T& value)
template<class Iter> Ref<Base> Ast::push(Iter first, Iter last)
Ref<Base> Ast::push(std::initializer_list<Ref<Base>> init)
```

Push elements to an existing Ast.

1. Encode and insert any type `T` for which `zz::Traits<T>` is specialized.
2. Build a new list from a range of handles.
3. Build a list from a list of handle.

### Element Access

```cpp
template<class T, class Base> T get(const Ast<Base>& ast, Ref<Base> pos) const
```

Dynamic cast of the referenced element to any type `T` for which
`zz::Traits<Base, T>` is specialized.  On failure throws `std::runtime_error`.

## zz::Emit

A tree and a position used for serialization

```cpp
template<class Base> struct Emit {
        const Ast<Base>& ast;
        Ref<Base> ref;
};
```

### Related functions

```cpp
template<class Base> Emit<Base> emit(const Ast<Base>& ast, Ref<Base> ref);
template<class Base> std::ostream& operator<<(std::ostream& out, const Emit<Base>& e);
```

## zz::Traits

Define how data is encoded in the tree and decoded from it.

```cpp
template<class Base, class T, class Enable = void> struct Traits {
        static Ref<Base> push(std::vector<Ref<Base>>&, const T&);
        static T get(std::vector<Ref<Base>>&, Ref ref);
};
```

The `push()` function encodes the given value into the array.

The `get()` function extracts a valid value of type `T` from the position
pointed by `ref` on the array; it extraction is not possible,
`std::runtime_error` is thrown.

The following specializations are provided

```cpp
template<class Base, class T> struct Traits<Base, T, std::enable_if_t<std::is_integral_v<T>>>;
template<class Base, class T> struct Traits<Base, T, std::enable_if_t<std::is_enum_v<T>>>;
template<class Base, class T> struct Traits<Base, T, std::enable_if_t<std::is_floating_point_v<T>>>;
template<class Base, class T> struct Traits<Base, std::string_view>;
template<class Base, class T> struct Traits<Base, std::string>;
template<class Base, class T> struct Traits<Base, char*>;
template<class Base, class T> struct Traits<Base, const char*>;
template<class Base> struct Traits<Base, List<Base>> ;
```

