# Installation

## Requirements

Zebu is available as both C and C++ header-only libraries.

The C version uses the C11 standard.

The C++ version uses the C++17 standard.

The included examples require GNU Bison.

## Makefiles

Zebu provices a simple Makefile with the following targets:

- `all`: only builds examples since the library is header-only.
- `clean`: see `all`
- `install`: install headers
- `uninstall`: delete installed headers
- `check`: run tests
