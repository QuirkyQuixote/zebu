
# C API

## Base type

The base type determines the size of the nodes in the tree; it gives an upper
size to the actual tree, but also to the numeric values stored on it.

By default, the base is set to `uint32_t`, and that gives a tree with up to
`2^32` nodes that can store integers and reals with up to 32 bits.

It can be changed by defining a new value for `ZZ_BASE` before including
`zebu.h`.

```c
#ifndef ZZ_BASE
#define ZZ_BASE uint32_t
#endif
```

## Node types

These define the possible node types

```c
#define ZZ_TINYINT      0
#define ZZ_INTEGER      ((ZZ_BASE)1 << (sizeof(ZZ_BASE) * 8 - 4))
#define ZZ_REAL         ((ZZ_BASE)2 << (sizeof(ZZ_BASE) * 8 - 4))
#define ZZ_STRING       ((ZZ_BASE)3 << (sizeof(ZZ_BASE) * 8 - 4))
#define ZZ_LIST         ((ZZ_BASE)4 << (sizeof(ZZ_BASE) * 8 - 4))
#define ZZ_MASK         (ZZ_TINYINT | ZZ_INTEGER | ZZ_REAL | ZZ_STRING | ZZ_LIST)

#define ZZ_NPOS         ((ZZ_BASE)-1)
```

## zz_ast

Abstract Syntax Tree, with all nodes stored as contiguously as possible

Keeps the actual tree packed in a single array, gives and takes handles that
encode the node type and offset as a way to interact with it.

```c
struct zz_ast {
        ZZ_BASE *data;
        ZZ_BASE size;
        ZZ_BASE capacity;
};
```

### Related Functions

```c
struct zz_ast zz_ast(void);
```

Construct a new `zz_ast`.

```c
void zz_destroy(struct zz_ast *ast);
```

Destroy `zz_ast`, deallocating all memory.

### Insertion

```c
ZZ_BASE zz_push(struct zz_ast* ast, ZZ_BASE* buf, ZZ_BASE len)
```

Push elements to the end of the tree, return the index of the first one
This is a low-level function used to implement the rest of the pushes.

```c
ZZ_BASE zz_push_integer(struct zz_ast* ast, long value);
ZZ_BASE zz_push_real(struct zz_ast* ast, double value);
ZZ_BASE zz_push_string(struct zz_ast* ast, const char* str, size_t len);
ZZ_BASE zz_push_list(struct zz_ast* ast, ...);
ZZ_BASE zz_push_buf(struct zz_ast* ast, struct zz_buf* buf);
```

Push elements of a specific type and return a typed handle to them.

`zz_push_integer` may allocate space for a big integer in the tree, or return
a handle that contains the small integer itself.

`zz_push_real` will allocate space for a copy of the given double and return
a handle pointing to it.

`zz_push_string` will allocate space for a copy of the given string and return
a handle pointing to it; if `len` is less than zero, the actual lenght will be
`strlen(str)`.

`zz_push_list` will build a list with the elements passed to it, that must be
valid handles of the same tree, and return a handle to it.

`zz_push_buf` will build a list with the elements of the given buffer, destroy
the buffer, and return a handle to the resulting list.

### Element Access

```c
ZZ_BASE zz_get(struct zz_ast* ast, ZZ_BASE pos);
```

Return node of the tree whose handle is pos.
This is a low-level function used to implement the rest of the getters.

```c
long zz_get_integer(struct zz_ast* ast, ZZ_BASE pos);
double zz_get_real(struct zz_ast* ast, ZZ_BASE pos);
const char* zz_get_string(struct zz_ast* ast, ZZ_BASE pos);
const zz_list* zz_get_list(struct zz_ast* ast, ZZ_BASE pos);
```

Return object of the tree whose handle is pos.
Abort execution of the object is not of the given type.

### Serialization

```c
void zz_print(struct zz_ast* ast, ZZ_BASE pos, FILE* out)
void zz_println(struct zz_ast* ast, ZZ_BASE pos, FILE* out)
```

Print a tree.

## zz_list

View for a list in the zz_ast instance

```c
struct zz_list {
        ZZ_BASE size;
        ZZ_BASE data[];
};
```

## zz_buf

Auxiliary data for list building

```c
struct zz_buf {
        ZZ_BASE size;
        ZZ_BASE capacity;
        ZZ_BASE data[];
};
```

```c
struct zz_buf* zz_append(struct zz_buf* buf, ZZ_BASE pos);
```

Push element to a buffer, reallocating if necessary, and return a pointer to
the new one. An empty buffer can be represented with a NULL pointer

