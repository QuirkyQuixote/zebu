# C Usage

## Requirements

To use the C implementation you must include `zebu4/zebu.h`.

```c
#include "zebu4/zebu.h"
```

A tree is created with `zz_ast()` and destroyed with `zz_destroy()`.

```c
struct zz_ast ast = zz_ast();
/* do things with ast */
zz_destroy(&ast);
```

## Constructing a Tree

Once the tree is created, you can push nodes into it and obtain handles to
them with the `zz_push...()` family of functions.

```c
ZZ_BASE integer = zz_push_integer(&ast, 42);
ZZ_BASE real = zz_push_real(&ast, 3.141592);
ZZ_BASE string = zz_push_string(&ast, "Hello, world!", -1);
ZZ_BASE list = zz_push_list(&ast, integer, real, string);
```

The `zz_push_list()` function is variadic and can build a list of any size in
just one function call, but you must know the number of elements of the list at
compile time. If you don't you can use `zz_buf` to make a temporary list.

```c
struct zz_buf *buf = NULL;
buf = zz_append(buf, integer);
buf = zz_append(buf, real);
buf = zz_append(buf, string);
ZZ_BASE list = zz_push_buf(&ast, buf);
```

You can also use non-negative integers or enum values mapped to non-negative
integers in place of handlers returned by a `zz_push...()` function, and
they'll become integer values in the tree.

```c
ZZ_BASE list = zz_push_list(&ast, TOKEN_ADD, 1, 2);
```

## Navigating a Tree

To retrieve the values stored in the tree, use the `zz_get...()` functions.

```c
printf("%d\n", zz_get_integer(&ast, integer));
printf("%g\n", zz_get_real(&ast, real));
printf("%s\n", zz_get_string(&ast, string));
```

Specifically, retrieving a list will return a `zz_list` object that can be used
to iterate over the list values.

```c
struct zz_list* l = zz_get_list(&ast, list);
printf("%d elements returned\n", l->size);
ZZ_BASE val;
zz_foreach (val, l) {
        /* do things with the values of list */
}
```

