
# Where this file resides

root_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# When version changes, increment this number

VERSION := 4.1.0

# Determine where we are going to install things

prefix := $(HOME)/.local
includedir := $(prefix)/include

# According to the GNU Make documentation: "Every Makefile should define the
# variable INSTALL, which is the basic command for installing a file into the
# system.  Every Makefile should also define the variables INSTALL_PROGRAM and
# INSTALL_DATA."

INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

YACC := bison

# Overridable flags

CFLAGS := -g
CXXFLAGS := -g

# Override these flags so they can't be re-overriden.

override CFLAGS += -std=c11
override CFLAGS += -MMD
override CFLAGS += -fPIC
override CFLAGS += -Wall
override CFLAGS += -Werror
override CFLAGS += -Wfatal-errors

override CXXFLAGS += -std=c++17
override CXXFLAGS += -MMD
override CXXFLAGS += -fPIC
override CXXFLAGS += -Wall
override CXXFLAGS += -Werror
override CXXFLAGS += -Wfatal-errors

override CPPFLAGS += -I$(root_dir)

override LDLIBS += -lstdc++


# Do not allow direct compilation of % from %.cc; it breaks -MMD

%: %.c
%: %.cc

# Use bison instead of yacc

%.c: %.y
	$(YACC) $< -o $@

%.cc: %.yy
	$(YACC) $< -o $@

# All tests to be compiled and executed
examples += examples/calc
examples += examples/conf
examples += examples/json

examples += examples/list++
examples += examples/conf++
examples += examples/json++

tests += tests/0000.conf
tests += tests/0001.conf
tests += tests/0002.conf
tests += tests/0003.conf

tests += tests/0000.json
tests += tests/0001.json
tests += tests/0002.json
tests += tests/0003.json
tests += tests/0004.json
tests += tests/0005.json
tests += tests/0006.json
tests += tests/0007.json
tests += tests/0008.json
tests += tests/0009.json
tests += tests/0010.json
tests += tests/0011.json
tests += tests/0012.json
tests += tests/0013.json
tests += tests/0014.json
tests += tests/0015.json
tests += tests/0016.json
tests += tests/0017.json
tests += tests/0018.json
tests += tests/0019.json
tests += tests/0020.json
tests += tests/0021.json

.PHONY: all
all: $(examples)

.PHONY: check
check: $(tests)

.PHONY: clean
clean:
	$(RM) $(tests)
	$(RM) tests/*.o
	$(RM) tests/*.d
	$(RM) $(examples)
	$(RM) examples/*.o
	$(RM) examples/*.d

.PHONY: install
install:
	$(INSTALL) -d $(DESTDIR)$(includedir)/zebu
	$(INSTALL_DATA) zebu/zebu4.h $(DESTDIR)$(includedir)/zebu
	$(INSTALL_DATA) zebu/zebu4_cpp.h $(DESTDIR)$(includedir)/zebu

.PHONY: uninstall
uninstall:
	-$(RM) $(DESTDIR)$(includedir)/zebu/zebu4.h
	-$(RM) $(DESTDIR)$(includedir)/zebu/zebu4_cpp.h
	-$(RM) -d $(DESTDIR)$(includedir)/zebu

tests/%.conf: FORCE
	-examples/conf < $@
	-examples/conf++ < $@

tests/%.json: FORCE
	-examples/json < $@
	-examples/json++ < $@

FORCE:

-include $(shell find -name "*.d")

